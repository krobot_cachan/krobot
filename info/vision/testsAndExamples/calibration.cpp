// calibration.cpp
//
// Programme de demonstration de la calibration de camera par OpenCV
// D'après le programme de "Learning OpenCV".
//
// Ce programme détermine les paramètres des déformations de la caméra,
// voir correctionMono.c pour la correction en elle-même.
//
// Convention d'appel :
// calibration webcam board_w board_h numbre_of_views
//
// Appuyer sur 'p' pour mettre/enlever la pause, ESC pour quitter

#include <cv.h>
#include <highgui.h>
#include <stdio.h>
#include <stdlib.h>

int n_boards = 0;
const int board_dt = 20; // Attendre 20 images entre 2 vues
int board_w;
int board_h;
int webcam;

int main(int argc, char * argv[]) {

  int i;

  if (argc != 5) {
    printf("ERREUR : mauvais nombre de paramètres\n\n");
    printf("\tUtilisation :\n");
    printf("calibration webcam board_w board_h numbre_of_views\n");
    return -1;
  }

  webcam = atoi(argv[1]);
  board_w = atoi(argv[2]);
  board_h = atoi(argv[3]);
  n_boards = atoi(argv[4]);

  int board_n = board_w * board_h;
  CvSize board_sz = cvSize(board_w, board_h);
  CvCapture* capture = cvCreateCameraCapture(webcam);

  if (capture == NULL) {
    printf("ERREUR : impossible d'ouvrir la webcam demandée");
    return -1;
  }

  cvNamedWindow("Calibration");
  // Creation de l'espace de stockage
  CvMat* image_points      = cvCreateMat(n_boards*board_n,2,CV_32FC1);
  CvMat* object_points     = cvCreateMat(n_boards*board_n,3,CV_32FC1);
  CvMat* point_counts      = cvCreateMat(n_boards,1,CV_32SC1);
  CvMat* intrinsic_matrix  = cvCreateMat(3,3,CV_32FC1);
  CvMat* distortion_coeffs = cvCreateMat(5,1,CV_32FC1);

  CvPoint2D32f* corners = new CvPoint2D32f[ board_n];
  int corner_count;
  int successes = 0;
  int step, frame = 0;

  IplImage *image = cvQueryFrame(capture);
  IplImage *gray_image = cvCreateImage(cvGetSize(image),8,1);

  // On boucle jusqu'à obtenir n_boards capture correctes
  // (càd avec tous les coins trouvés)
  while(successes < n_boards) {
    // On saute board_dt images pour laisser à l'utilisateur le
    // temps de déplacer l'échiquier
    if (frame++ % board_dt == 0) {
      // On cherche les coins de l'échiquier
      int found = cvFindChessboardCorners(image, board_sz, corners, &corner_count,
					  CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS
					  );

      // On détermine les coins avec une précision subpixel
      cvCvtColor(image, gray_image, CV_BGR2GRAY);
      cvFindCornerSubPix(gray_image, corners, corner_count, cvSize(11,11),
			 cvSize(-1,-1), cvTermCriteria(CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 30, 0.1)
			 );

      // On le dessine
      cvDrawChessboardCorners(image, board_sz, corners, corner_count, found);
      cvShowImage("Calibration", image);

      // Si l'on a récupéré une bonne image, on l'ajouter à nos données
      if (corner_count == board_n) {
	step = successes*board_n;
	for(int i=step, j=0; j<board_n; ++i,++j) {
	  CV_MAT_ELEM(*image_points, float, i, 0) = corners[j].x;
	  CV_MAT_ELEM(*image_points, float, i, 1) = corners[j].y;
	  CV_MAT_ELEM(*object_points, float, i, 0) = j/board_w;
	  CV_MAT_ELEM(*object_points, float, i, 1) = j%board_w;
	  CV_MAT_ELEM(*object_points, float, i, 2) = 0.0f;
	}
	CV_MAT_ELEM(*point_counts, int, successes, 0) = board_n;
	successes++;

	// On affiche les statistiques
	printf("Image correcte n°%d obtenue, %d coins trouvés\n", successes, corner_count);
      }
      else {
	printf("Image incorrecte, %d coins trouvés\n", corner_count);
      }
    } // fin traitement de l'image

    // Traitement des intéractions utilisateur
    int c = cvWaitKey(15);
    if (c=='p') {
      c = 0;
      while(c != 'p' && c != 27)
	c = cvWaitKey(250);
    }
    if (c==27)
      return 0;

    // Image suivante
    image = cvQueryFrame(capture);
  } // Fin de récupération des images correctes

  // Allocation des matrices en fonction du nombre d'échiquiers trouvés
  CvMat* object_points2 = cvCreateMat(successes*board_n,3,CV_32FC1);
  CvMat* image_points2  = cvCreateMat(successes*board_n,2,CV_32FC1);
  CvMat* point_counts2  = cvCreateMat(successes,1,CV_32SC1);

  // Transfert des points vers les matrices de dimensions correctes
  for(int i = 0; i < successes*board_n; ++i) {
    CV_MAT_ELEM(*image_points2, float, i, 0) =
      CV_MAT_ELEM(*image_points, float, i, 0);
    CV_MAT_ELEM(*image_points2, float, i, 1) =
      CV_MAT_ELEM(*image_points, float, i, 1);
    CV_MAT_ELEM(*object_points2, float, i, 0) =
      CV_MAT_ELEM(*object_points, float, i, 0);
    CV_MAT_ELEM(*object_points2, float, i, 1) =
      CV_MAT_ELEM(*object_points, float, i, 1);
    CV_MAT_ELEM(*object_points2, float, i, 2) =
      CV_MAT_ELEM(*object_points, float, i, 2);
  }
  for(int i = 0; i < successes; ++i) {
    // Ces nombres sont tous égaux
    CV_MAT_ELEM(*point_counts2, int, i, 0) =
      CV_MAT_ELEM(*point_counts, int, i, 0);
  }

  cvReleaseMat(&object_points);
  cvReleaseMat(&image_points);
  cvReleaseMat(&point_counts);

  // On a maintenant tous les coins d'échiquier nécessaires
  // On initialise la matrice intrinsèque de manière à ce que
  // les 2 longueurs focales aient un ratio de 1.0
  CV_MAT_ELEM(*intrinsic_matrix, float, 0, 0) = 1.0f;
  CV_MAT_ELEM(*intrinsic_matrix, float, 1, 1) = 1.0f;

  printf("Calibration\n");
  // Calibration de la caméra
  cvCalibrateCamera2(object_points2, image_points2,
		     point_counts2, cvGetSize(image),
		     intrinsic_matrix, distortion_coeffs,
		     NULL, NULL, 0 // CV_CALIB_FIX_ASPECT_RATIO
		     );

  printf("Calibration terminée\n");
  printf("Sauvegarde du résultat dans Intrinsics.xml et Distortion.xml\n");
  printf("\t--- Lancez correctionMono pour voir le résultat ! ---\n");
  

  // Sauvegarde de la matrice instrinsèque et du vecteur de distorsion
  cvSave("Intrinsics.xml", intrinsic_matrix);
  cvSave("Distortion.xml", distortion_coeffs);

  return 0;
}
