// histogramBackProjection.cpp
//
// Programme de demonstration de la reconnaissance de couleurs par histogramme
// D'après le programme de "Learning OpenCV".
//
//
// Convention d'appel :
// histogramBackProjection template webcam patch_type patchDim
//
#include <cv.h>
#include <cxcore.h>
#include <highgui.h>
#include <stdio.h>

void help(){
printf("\nConvention d'appel:\n"
       "histogramBackProjection template webcam patch_type patchDim\n"
       "     template : image de l'objet à rechercher\n"
       "     webcam   : webcam dans laquelle rechercher l'objet\n"
       "     patch_type : permet de choisir la méthode à utiliser\n"
       "                  0 Correlation, 1 ChiSqr, 2 Intersect, 3 Bhattacharrya\n\n");
}

// Apprend l'histogramme de la l'image modèle, puis "déprojectte" les images de la webcam
//  
//  Si patch_type est présent, on realise cvCalcBackProjectPatch()
//     patch_type peut prendre les valeurs suivantes :
//     0 Correlation, 1 ChiSqr, 2 Intersect, 3 Bjattacharyya
//  Sinon, on réalise cvCalcBackProject().
// 
int main( int argc, char** argv ) {

  IplImage* src[2],*dst=0,*ftmp=0;
  CvCapture *capture = NULL;
  int i,type = 0;
  int patch = 0; //cvCalcBackProject() par défaut
  int patch_dim = 61; // Valeur par défaut dans l'exemple initial
  if( argc >= 3){
    capture = cvCreateCameraCapture(atoi(argv[2]));
    if (capture == NULL) {
      printf("ERREUR : impossible d'ouvrir la webcam demandée");
      return -1;
    }
    if(argc > 3) {
      patch = 1;
      type = atoi(argv[3]); //On passe en cvCalcBackProjecPatch() avec le type désiré
      if(argc > 4) {
	patch_dim = atoi(argv[4]);
      }
    }
    printf("Patch = %d, type = %d\n",patch,type);
    // On charge le modèle
    src[0] = 0; src[1] = 0;
    if((src[0]=cvLoadImage(argv[1], 1))== 0) {
      printf("Impossible de lire le modèle : %s\n",argv[i]);
      return(-1);
    }
    src[1] = cvQueryFrame(capture);
    
    // Conversion de l'image en HSV et décomposition en différents plans de couleur
    //
    IplImage *hsv[2], *h_plane[2],*s_plane[2],*v_plane[2],*planes[2][2]; 
    IplImage* hist_img[2];
    CvHistogram* hist[2];
    // int h_bins = 30, s_bins = 32; 
    int h_bins = 16, s_bins = 16;
    int    hist_size[] = { h_bins, s_bins };
    float  h_ranges[]  = { 0, 180 };          // la teinte est normalisée sur [0,180]
    float  s_ranges[]  = { 0, 255 }; 
    float* ranges[]    = { h_ranges, s_ranges };
    int scale = 10;
    if(patch){
      int iwidth = src[1]->width - patch_dim + 1;
      int iheight = src[1]->height - patch_dim + 1;
      ftmp = cvCreateImage( cvSize(iwidth,iheight),32,1);
      cvZero(ftmp);
    }
    dst = cvCreateImage( cvGetSize(src[1]),8,1);
    
    cvZero(dst);
    hsv[0] = cvCreateImage( cvGetSize(src[0]), 8, 3 );
    hsv[1] = cvCreateImage( cvGetSize(src[1]), 8, 3 );
    cvCvtColor( src[0], hsv[0], CV_BGR2HSV );
    
    h_plane[0]  = cvCreateImage( cvGetSize(src[0]), 8, 1 );
    s_plane[0]  = cvCreateImage( cvGetSize(src[0]), 8, 1 );
    v_plane[0]  = cvCreateImage( cvGetSize(src[0]), 8, 1 );
    h_plane[1]  = cvCreateImage( cvGetSize(src[1]), 8, 1 );
    s_plane[1]  = cvCreateImage( cvGetSize(src[1]), 8, 1 );
    v_plane[1]  = cvCreateImage( cvGetSize(src[1]), 8, 1 );
    planes[0][0] = h_plane[0];
    planes[0][1] = s_plane[0];
    planes[1][0] = h_plane[1];
    planes[1][1] = s_plane[1];
    cvCvtPixToPlane( hsv[0], h_plane[0], s_plane[0], v_plane[0], 0 );
    // Contructions de l'histogramme du modèle et calcul de son contenu
    //
    hist[0] = cvCreateHist( 
			   2, 
			   hist_size, 
			   CV_HIST_ARRAY, 
			   ranges, 
			   1 
			    ); 
    cvCalcHist( planes[0], hist[0], 0, 0 );
    if(patch)
      cvNormalizeHist( hist[0], 1.0 ); //On ne normalise pas pour cvCalcBackProject(), 
    
    // Affichage du motif
    cvNamedWindow("Motif");
    cvShowImage("Motif", src[0]);
    
    cvNamedWindow("Image source");
    cvShowImage("Image source", src[1]);
    cvNamedWindow("Detection");
    
    // Maintenant on analyse le flux vidéo de la webcam
    do {
      src[1] = cvQueryFrame(capture);
      
      // Décomposition HSV de l'image capturée
      cvCvtColor(src[1], hsv[1], CV_BGR2HSV);
      cvCvtPixToPlane(hsv[1], h_plane[1], s_plane[1], v_plane[1], 0);
      
      // Projection inverse
      if((patch)) {
	printf("Exécution de cvCalcBackProjectPatch() avec le type =%d\n",type);
	cvCalcBackProjectPatch(planes[1],ftmp,cvSize(patch_dim,patch_dim),hist[0],type,1.0);
	printf("Pixels non nul de ftmp = %d\n",cvCountNonZero(ftmp));
	
      }else {
	printf("Exécution de cvCalcBackProject()\n");
	cvCalcBackProject(planes[1],dst,hist[0]);
      }
      
      // Affichage
      cvShowImage("Image source", src[1]);
      if (patch) {
	cvShowImage("Detection", ftmp );
      }
      else {
	cvShowImage("Detection", dst);
      }
      //} while(cvWaitKey(15) == 0);
      cvWaitKey(25);
    } while(1);
      
  }
  else { printf("Erreur: Mauvais nombre d'arguments !\n"); help(); return -1;}
}
