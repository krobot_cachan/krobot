// calibration.c
//
// Programme de demonstration de la calibration de camera par OpenCV
// D'après le programme de "Learning OpenCV".
//
// Convention d'appel :
// calibration webcam board_w board_h numbre_of_views
//
// Appuyer sur 'p' pour mettre/enlever la pause, ESC pour quitter

#include <cv.h>
#include <highgui.h>
#include <stdio.h>
#include <stdlib.h>

int webcam;

int main(int argc, char * argv[]) {

  int i;

  if (argc >= 2)
    webcam = atoi(argv[1]);
  else
    webcam = 1;

  CvCapture* capture = cvCreateCameraCapture(webcam);

  if (capture == NULL) {
    printf("ERREUR : impossible d'ouvrir la webcam demandée");
    return -1;
  }

  IplImage *image = cvQueryFrame(capture);

  // Chargement des matrices des déformations :
  CvMat *intrinsic  = (CvMat*)cvLoad("Intrinsics.xml");
  CvMat *distortion = (CvMat*)cvLoad("Distortion.xml");

  // Construit la carte de correction de l'image que l'on va utiliser
  // pour toutes les images suivantes
  IplImage* mapx = cvCreateImage(cvGetSize(image), IPL_DEPTH_32F, 1);
  IplImage* mapy = cvCreateImage(cvGetSize(image), IPL_DEPTH_32F, 1);
  cvInitUndistortMap(intrinsic,
		     distortion,
		     mapx,
		     mapy
		     );

  // On lit les images de caméra et on affiche les images brutes
  // et redressées
  cvNamedWindow("Raw");
  cvNamedWindow("Undistort");

  while(image) {
    IplImage *t = cvCloneImage(image);
    cvShowImage("Raw", image); // Image brute
    cvRemap(t, image, mapx, mapy);     // Redressement de l'image
    cvReleaseImage(&t);
    cvShowImage("Undistort", image);   // Image corrigée

    // Gestion de l'intéraction avec l'utilisateur
    int c = cvWaitKey(15);
    if (c == 'p') {
      c = 0;
      while (c != 'p' && c != 27)
	c = cvWaitKey(250);
    }
    if (c == 27)
      break;

    // Image suivante
    image = cvQueryFrame(capture);
  }

  return 0;
}
