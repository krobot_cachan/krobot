open Urg_simple

let t = init ()

let rad_table = Array.init (Bigarray.Array1.dim t.data)
    (fun i -> Urg.urg_index2rad t.urg i)

open Format

let print_rad_table =
  printf "@[<1>[|";
  for i = 0 to Array.length rad_table - 1 do
    printf "%.30f;@ " rad_table.(i);
  done;
  printf "@]|]@."
