open Lwt
open Urg_simple

let t = init ()

let min_val = 30

let rad_table = Array.init (Bigarray.Array1.dim t.data)
    (fun i -> Urg.urg_index2rad t.urg i)

let less a b =
  if a < min_val
  then false
  else if b < min_val
  then true
  else a < b

let ba_extrema ba cmp =
  let m = ref (Nativeint.to_int ba.{0}) in
  let pos = ref 0 in
  for i = 0 to Bigarray.Array1.dim ba - 1 do
    let v = Nativeint.to_int ba.{i} in
    if cmp v !m
    then (m := v; pos := i)
  done;
  !m, !pos

let count = ref 0

let get' t =
  let t1 = Unix.gettimeofday () in
  let c1 = !count in
  Lwt_preemptive.detach get t >>= fun v ->
  let t2 = Unix.gettimeofday () in
  let c2 = !count in
  Printf.printf "diff count %i %f\n%!" (c2 - c1) (t2 -. t1);
  return v

let () =
  let rec aux () =
    get' t >>= fun ts ->
    let min, pos_min = ba_extrema t.data less in
    let rad_min = rad_table.(pos_min) in
    Printf.printf "%i %i %f %i %i\n%!" min pos_min rad_min ts !count;
    aux ()
  in
  let rec loop () =
    incr count;
    Lwt_unix.sleep 0.001 >>= fun () ->
    loop ()
  in
  let t1 = aux () in
  let _ = loop () in
  Lwt_unix.run t1
