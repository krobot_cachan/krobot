open Urg_simple

let t = init ()

let min_val = 30

let rad_table = Array.init (Bigarray.Array1.dim t.data)
    (fun i -> Urg.urg_index2rad t.urg i)

let less a b =
  if a < min_val
  then false
  else if b < min_val
  then true
  else a < b

let ba_extrema ba cmp =
  let m = ref (Nativeint.to_int ba.{0}) in
  let pos = ref 0 in
  for i = 0 to Bigarray.Array1.dim ba - 1 do
    let v = Nativeint.to_int ba.{i} in
    if cmp v !m
    then (m := v; pos := i)
  done;
  !m, !pos

let () = Printf.printf "id: %s\n%!" t.id

let () =
  for i = 0 to 1000 do
    let ts = get t in
    let min, pos_min = ba_extrema t.data less in
    let max, pos_max = ba_extrema t.data ((>):int -> int -> bool) in
    let rad_min = rad_table.(pos_min) in
    Printf.printf "%i %i %f %i\n%!" min pos_min rad_min ts
  done
