type urg

external urg_disconnect : urg -> unit = "ocaml_urg_disconnect"
external urg_connect : string -> int -> urg option = "ocaml_urg_connect"

type point_data = (nativeint, Bigarray.nativeint_elt, Bigarray.c_layout) Bigarray.Array1.t

let make_point_data n =
  Bigarray.Array1.create Bigarray.nativeint Bigarray.c_layout n

type typ =
  | URG_GD
  | URG_GD_INTENSITY
  | URG_GS
  | URG_MD
  | URG_MD_INTENSITY
  | URG_MS

external urg_dataMax : urg -> int = "ocaml_urg_dataMax"
external urg_requestData : urg -> typ -> int = "ocaml_urg_requestData"
external urg_receiveData : urg -> point_data -> int = "ocaml_urg_receiveData"
external urg_recentTimestamp : urg -> int = "ocaml_urg_recentTimestamp"

external urg_scanMsec : urg -> int = "ocaml_urg_scanMsec"
(* in milliseconds *)

external urg_maxDistance : urg -> int = "ocaml_urg_maxDistance"
(* in millimeters *)

external urg_minDistance : urg -> int = "ocaml_urg_minDistance"
(* in millimeters *)

external urg_setCaptureTimes : urg -> int -> int = "ocaml_urg_setCaptureTimes"
(* work only with captures MD and MS:
   set to 0 for continuous scanning *)

external urg_remainCaptureTimes : urg -> int = "urg_remainCaptureTimes"
(* number of captures scheduled and not done *)

external urg_index2rad : urg -> int -> float = "ocaml_urg_index2rad"
external urg_reboot : urg -> int = "ocaml_urg_reboot"

external urg_versionLines : urg -> string array option = "ocaml_urg_versionLines"
