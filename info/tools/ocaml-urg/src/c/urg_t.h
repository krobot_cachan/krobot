#ifndef QRK_C_URG_T_H
#define QRK_C_URG_T_H

/*!
  \file
  \brief Structure for URG control

  \author Satofumi KAMIMURA

  $Id: urg_t.h 1627 2010-01-18 00:29:39Z satofumi $
*/

#include "urg_parameter_t.h"
#include "serial_t.h"


/*!
  \brief Constant for URG control
*/
typedef enum {
  UrgLaserOff = 0,
  UrgLaserOn,
  UrgLaserUnknown,
} urg_laser_state_t;


/*!
  \brief Structure for URG control
*/
typedef struct {

  serial_t serial_;              /*!< Structure of serial control */
  int errno_;                    /*!< Store error number */
  urg_parameter_t parameters_;   /*!< Sensor parameter */

  int skip_lines_;   /*!< Number of lines to be skipped in one scan */
  int skip_frames_; /*!< Number of scans to be skipped(Apply only to MD/MS) */
  int capture_times_; /*!< Frequency of data acquisition(Apply only to MD/MS) */

  urg_laser_state_t is_laser_on_; /*!< 0 when laser is turned off */

  long last_timestamp_;         /*!< Final time stamp */
  int remain_times_;            /*!< データ取得の残り回数 */

  char remain_data_[3];         /*! 次の行で処理するデータ */
  int remain_byte_;             /*! 次の行で処理するデータ数 */

} urg_t;

#endif /* !QRK_C_URG_T_H */
