open Urg

type t =
  { urg : urg;
    data : Urg.point_data;
    id : string; }

let init ?(tty="/dev/ttyACM0") () =
  match urg_connect tty 115200 with
  | None -> failwith "fail connect"
  | Some urg ->
    match urg_versionLines urg with
    | None ->
      failwith "Couldn't get id"
    | Some lines ->
      let data = make_point_data (urg_dataMax urg) in
      if urg_setCaptureTimes urg 0 < 0
      then failwith "set capture times";
      if urg_requestData urg URG_MD < 0
      then failwith "request data";
      let id = String.sub lines.(4) 5 8 in
      { urg; data; id }

let get t =
  if urg_receiveData t.urg t.data < 0
  then failwith "receive data";
  let ts = urg_recentTimestamp t.urg in
  ts
